CREATE TABLE IF NOT EXISTS catalog (
    id serial PRIMARY KEY,
    name varchar NOT NULL,
    price float NOT NULL,
    category varchar NOT NULL,
    createdDate date DEFAULT CURRENT_DATE,
    updatedDate date DEFAULT CURRENT_DATE
);

CREATE TABLE IF NOT EXISTS roles (
    id serial PRIMARY KEY,
    value varchar NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS users (
    id serial PRIMARY KEY,
    username varchar NOT NULL UNIQUE,
    password varchar NOT NULL,
    role_id integer REFERENCES roles(id)
);

INSERT INTO roles (value) VALUES ('Admin');
INSERT INTO roles (value) VALUES ('Regular User');