const {
    ServerError
} = require('../errors');
const validator = require('validator');

/**
 * @param {[String]} keys
 * @param {*} obj
 */
const unpack = (keys, obj) => (keys.reduce((acc, val) => ({
    ...acc,
    [val]: obj[val]
}), {}));

/**
 *
 * @param {Object} params
 * @param {String} type
 */
const verifyParamsByType = (params, type, verifyExists = true, complexType = false) => {
    for (let i in params) {
        if (params[i] === undefined || params[i] === null) {
            if (!verifyExists) {
                return;
            }
            throw new ServerError(`${i} is missing!`, 400);
        }
        switch (type) {
            case 'string': {
                if (!validator.isAlpha(params[i])) {
                    throw new ServerError(`${i} is not string!`, 400);
                }
                break;
            }
            case 'alphanumeric': {
                if (!validator.isAlphanumeric(params[i])) {
                    throw new ServerError(`${i} does not contain only letters and numbers!`, 400);
                }
                break;
            }
            case 'date': {
                if (!validator.isISO8601(params[i])) {
                    throw new ServerError(`${i} is not a valid date!`, 400);
                }
                break;
            }
            case 'ascii': {
                if (!validator.isAscii(params[i])) {
                    throw new ServerError(`${i} is not an ASCII construction!`, 400);
                }
                break;
            }
            case 'boolean': {
                if (!validator.isBoolean(params[i].toString())) {
                    throw new ServerError(`${i} is not boolean!`, 400);
                }
                break;
            }
            case 'int': {
                if (!validator.isInt(params[i].toString())) {
                    throw new ServerError(`${i} is not int!`, 400);
                }
                break;
            }
            case 'generalNumber': {
                if (typeof params[i] !== 'number') {
                    throw new ServerError(`${i} is not number`, 400)
                }
                break;
            }
            case 'genneralStrings': {
                if (typeof params[i] !== 'string') {
                    throw new ServerError(`${i} is not string`, 400)
                }
                break;
            }
            case 'jwt': {
                if (!validator.isJWT(params[i])) {
                    throw new ServerError(` ${i} is not jwt`, 400);
                }
                break;
            }
        }
    }
};

module.exports = {
    unpack,
    verifyParamsByType
};
