const {
    query
} = require('../data');

const {
    ServerError
} = require('../errors');

const add = async (name, price, category) => {
    await query('INSERT INTO catalog (name, price, category) VALUES ($1, $2, $3)', [name, price, category]);
};

const modifyProduct = async(id, name, price, category) => {
    await query('UPDATE catalog SET name = ($1), price = ($2), category = ($3), updateddate = CURRENT_DATE WHERE id = ($4)', [name, price, category, id]);
};

const getProducts = async() => {
    return await query('SELECT * FROM catalog');
};

const deleteProduct = async(id) => {
    await query('DELETE FROM catalog WHERE id = ($1)', [id]);
};


module.exports = {
    add,
    modifyProduct,
    getProducts,
    deleteProduct
};