const express = require('express');
const rateLimit = require("express-rate-limit");

const limiter = rateLimit({
    windowMs: 60 * 1000, // 1 minute
    max: 10 // limit each IP to 10 requests per windowMs
  });

const ProductsService = require('./services.js');

const {
    ServerError
} = require('../errors');

const {
    verifyParamsByType
} = require('../utils');

const {
    authorizeAndExtractToken
} = require('../security/Jwt')

const {
    authorizeRoles
} = require('../security/Roles')

const router = express.Router();

router.post('/', authorizeAndExtractToken, authorizeRoles('Admin'), limiter, async (req, res, next) => {
    const {
        name,
        price,
        category
    } = req.body;

    try {
        verifyParamsByType([name, category], 'genneralStrings');
        verifyParamsByType([price], 'int');

        await ProductsService.add(name, price, category);

        res.status(201).end();
    } catch (err) {
        next(err);
    }
});

router.put('/', authorizeAndExtractToken, authorizeRoles('Admin'), limiter, async (req, res, next) => {
    const {
        id,
        name,
        price,
        category
    } = req.body;

    try {
        verifyParamsByType([name, category], 'genneralStrings');
        verifyParamsByType([price, id], 'int');

        await ProductsService.modifyProduct(id, name, price, category);

        res.status(201).end();
    } catch (err) {
        next(err);
    }
});

router.get('/', authorizeAndExtractToken, authorizeRoles('Admin', 'Regular User'), limiter, async (req, res, next) => {
    try {
        res.json(await ProductsService.getProducts());
    } catch (err) {
        next(err);
    }
});

router.put('/', authorizeAndExtractToken, authorizeRoles('Admin'), limiter, async (req, res, next) => {
    const {
        id,
        name,
        price,
        category
    } = req.body;

    try {
        verifyParamsByType([name, category], 'genneralStrings');
        verifyParamsByType([price, id], 'int');

        await ProductsService.modifyProduct(id, name, price, category);

        res.status(201).end();
    } catch (err) {
        next(err);
    }
});

router.delete('/', authorizeAndExtractToken, authorizeRoles('Admin'), limiter, async (req, res, next) => {
    const {
        id,
    } = req.body;

    try {
        verifyParamsByType([id], 'int');

        await ProductsService.deleteProduct(id);

        res.end();
    } catch (err) {
        next(err);
    }
});

module.exports = router;