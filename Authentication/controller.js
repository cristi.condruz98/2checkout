const express = require('express');
const rateLimit = require("express-rate-limit");

const limiter = rateLimit({
    windowMs: 60 * 1000, // 1 minute
    max: 10 // limit each IP to 10 requests per windowMs
  });

const UsersService = require('./services.js');

const {
    ServerError
} = require('../errors');

const {
    unpack,
    verifyParamsByType
} = require('../utils');

const router = express.Router();

router.post('/register', limiter, async (req, res, next) => {
    const {
        username,
        password,
        role_id
    } = req.body;

    try {
        verifyParamsByType([username, password], 'genneralStrings');
        verifyParamsByType([role_id], 'int');
        
        await UsersService.add(username, password, role_id);
        
        res.status(201).end();
    } catch (err) {
        next(err);
    }

});

router.post('/login', async (req, res, next) => {
    const {
        username,
        password
    } = req.body;
  
    try {
      verifyParamsByType([username, password], 'genneralStrings');
      const token = await UsersService.authenticate(username, password);
  
      res.status(200).json(token);
  } catch (err) {
      next(err);
  }
});  

router.get('/', async (req, res, next) => {
    res.json(await UsersService.getUsers());
});

router.get('/roles', async (req, res, next) => {
    res.json(await UsersService.getRoles());
});

module.exports = router;
