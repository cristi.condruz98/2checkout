const {
    query
} = require('../data');

const {
    generateToken,
} = require('../security/Jwt');

const {
    ServerError
} = require('../errors');

const {
    hash,
    compare
} = require('../security/Password');

const addRole = async (value) => {
    await query('INSERT INTO roles (value) VALUES ($1)', [value]);
};

const getRoles = async() => {
    return await query('SELECT * FROM roles');
};

const getUsers = async() => {
    return await query('SELECT * FROM users');
};

const add = async (username, password, role_id) => {
    const hashed_passwd = await hash(password);
    await query('INSERT INTO users (username, password, role_id) VALUES ($1, $2, $3)', [username, hashed_passwd, role_id]);
};

const authenticate = async (username, password) => {
    const result = await query(`SELECT u.id, u.password, r.value as role FROM users u 
                                JOIN roles r ON r.id = u.role_id
                                WHERE u.username = $1`, [username]);
    if (result.length === 0) {
        throw new ServerError(`Username ${username} not found!`, 400);
    }
    const user = result[0];
    const hashed_pw = result[1];
    const user_role = result[2];
    const payload = {user, user_role};
    
    if(!compare(password, hashed_pw))
        throw new ServerError(`Wrong password!`, 400);
    
    return generateToken(payload)
};

module.exports = {
    add,
    addRole,
    getRoles,
    getUsers,
    authenticate
}
