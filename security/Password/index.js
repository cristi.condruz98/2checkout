const bcryptjs = require('bcryptjs');

const hash = async (plainTextPassword) => {
    const salt = await bcryptjs.genSalt(5);
    const hash = await bcryptjs.hash(plainTextPassword, salt);
    return hash

};

const compare = async (plainTextPassword, hashedPassword) => {
    try{
        const isOk = await bcryptjs.compare(plainTextPassword, hashedPassword);
        return isOk;
    } catch(err){
        return false;
    }
};

module.exports = {
    hash,
    compare
}