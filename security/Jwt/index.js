const jwt = require('jsonwebtoken');

const {
    ServerError
} = require('../../errors');

const {
    verifyParamsByType
} = require('../../utils');

const options = {
    issuer: process.env.JWT_ISSUER,
    subject: process.env.JWT_SUBJECT,
    audience: process.env.JWT_AUDIENCE
};

const generateToken = async (payload) => {
    try {
        const coded = await jwt.sign(payload, process.env.JWT_SECRET_KEY, options)
        return coded;
    } catch (err) {
        console.trace(err);
        throw new ServerError("Error encrypting token!", 400);
    }
};

const verifyAndDecodeData = async (token) => {
    try {
        const decoded = await jwt.verify(token, process.env.JWT_SECRET_KEY, options);
        return decoded;
    } catch (err) {
        console.trace(err);
        throw new ServerError("Error decrypting token!", 400);
    }
};

const authorizeAndExtractToken = async (req, res, next) => {
    try {
        if (!req.headers.authorization) {
            throw new ServerError('Missing authorization header!', 403);
        }
        const token = req.headers.authorization.split(" ")[1];

        verifyParamsByType([token], 'jwt');

        const decoded = await verifyAndDecodeData(token);
        
        req.state = {
            decoded
        };

        next();
    } catch (err) {
        next(err);
    }
};

module.exports = {
    generateToken,
    authorizeAndExtractToken
};