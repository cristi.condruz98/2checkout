# 2checkout

2Checkout BackendDev Assesment

- [x] Task 1 CRUD

Create a RESTful API application that exposes an endpoint to manage a catalog product.
The product fields are Id, Name, Price, Category, CreatedDate, UpdatedDate.

- [x] Task 2 Authentication

Protect the endpoint with some form of authentication. 
You’re free to choose whatever method you want basic, Bearer, Digest, OAuth etc.

- [x] Task 3 Rate limit

Make sure this endpoint can’t be abused by "bad" people. Rate limit based on number of requests in a certain time period



The database was implemented using a image of PostgreSQL and for ease of management, the docker-compose file also lists pgadmin. 
To get the database and the pgadmin images up and running use

```
docker-compose up
```

The database initially contains 3 tables. Catalog, roles and users. I initialized the roles table with 2 roles, *Admin* and *Regular User*

Only an Admin can create and update products in catalog.

To start the backend application, use

```
npm run start
```

To configure the ports the backend application will use, modify the ``` .env ``` file. For the database, modify the ```docker-compose.yml``` file. The defaults are 3000 for the backend application, 30001 for pgadmin, and 5432 for the postgresql image.

A user registers with with a POST request to /api/v1/auth/register with a JSON 

`{
	"username":"User1",
	"password":"Pass1",
	"role_id":"1"
}`

When the user tries to login with a POST request to /api/v1/auth/login, he will receive a JWT token that will be used for further requests. 
The CRUD requests are sent to /api/v1/auth/products . 

The requests are limited to 10 requests per minute. 

