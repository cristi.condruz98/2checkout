const Router = require('express').Router();

const ProductsController = require('../Products/controller.js');
const AuthenticationController = require('../Authentication/controller.js');

Router.use('/products', ProductsController);
Router.use('/auth', AuthenticationController);

module.exports = Router;